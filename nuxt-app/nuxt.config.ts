// nuxt.config.ts
import { defineNuxtModule } from 'nuxt'

export default defineNuxtConfig({
  devtools: { enabled: true },
  modules: ['nuxt-swiper'],
  swiper: {
    // Swiper options
    //----------------------
    // prefix: 'Swiper',
    // styleLang: 'css',
    // modules: ['navigation', 'pagination'], // all modules are imported by default
  }
})