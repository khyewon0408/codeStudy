import { createApp } from 'vue'
import App from './App.vue'

/*import { createRouter, createWebHistory } from 'vue-router'; // Vue Router를 import합니다.
import routes from './router/index.js'; // 라우트 설정이 있는 파일을 import합니다.

const router = createRouter({
  history: createWebHistory(),
  routes, // 라우트 설정 파일을 전달합니다.
});

createApp(App).use(router).mount('#app');
*/
createApp(App).mount('#app');
