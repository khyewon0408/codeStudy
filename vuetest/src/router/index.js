import { createRouter, createWebHistory } from 'vue-router';
import SearchResult from '../views/SearchResult.vue';

const routes = [
  {
    path: '/searchResult',
    name: 'SearchResult',
    component: SearchResult
  }
];

const router = createRouter({
    history: createWebHistory(),
  routes
});

export default router;